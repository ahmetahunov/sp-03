package ru.ahmetahunov.sp.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import ru.ahmetahunov.sp.model.enumerated.Status;
import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "app_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Task extends AbstractEntity {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    @Column(name = "start_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate = new Date(0);

    @NotNull
    @Column(name = "finish_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date finishDate = new Date(0);

    @NotNull
    @Column(name = "creation_date")
    private Date creationDate = new Date(System.currentTimeMillis());

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    @JoinColumn(name = "project_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Project project;

}
