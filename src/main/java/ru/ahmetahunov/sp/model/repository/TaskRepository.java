package ru.ahmetahunov.sp.model.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import ru.ahmetahunov.sp.model.entity.Task;
import javax.persistence.QueryHint;
import java.util.List;

public interface TaskRepository extends JpaRepository<Task, String> {

    @NotNull
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public List<Task> findAllTasksByProjectId(@NotNull String projectId);

}
