package ru.ahmetahunov.sp.model.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.model.entity.AbstractEntity;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import java.util.List;

public interface IAbstractService<T extends AbstractEntity> {

    public T persist(T item) throws InterruptedOperationException;

    public T merge(T item) throws InterruptedOperationException;

    @Nullable
    public T findOne(String id);

    @NotNull
    public List<T> findAll();

    public void remove(String id) throws InterruptedOperationException;

}
