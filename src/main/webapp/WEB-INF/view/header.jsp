<header>
    <nav>
        <a href="${pageContext.request.contextPath}/"><div class="title">TASK MANAGER</div></a>
        <a href="${pageContext.request.contextPath}/" class="link">HOME</a>
        <div class="brake">|</div>
        <a href="${pageContext.request.contextPath}/project/" class="link">PROJECT</a>
        <div class="brake">|</div>
        <a href="${pageContext.request.contextPath}/task/" class="link">TASK</a>
    </nav>
</header>
