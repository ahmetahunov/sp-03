package ru.ahmetahunov.sp.model.api.service;

import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.sp.model.entity.Task;
import java.util.List;

public interface ITaskService extends IAbstractService<Task> {

    @NotNull
    public List<Task> findAll(String projectId);

}
