package ru.ahmetahunov.sp.model.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.sp.model.repository.TaskRepository;
import ru.ahmetahunov.sp.model.api.service.ITaskService;
import ru.ahmetahunov.sp.model.entity.Task;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class TaskService implements ITaskService {

    @Setter
    @NotNull
    @Autowired
    private TaskRepository repository;

    @Override
    public Task persist(@Nullable final Task task) throws InterruptedOperationException {
        if (task == null) throw new InterruptedOperationException();
        return repository.save(task);
    }

    @Override
    public Task merge(@Nullable final Task task) throws InterruptedOperationException {
        if (task == null) throw new InterruptedOperationException();
        return repository.save(task);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findById(id).orElse(null);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllTasksByProjectId(projectId);
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        repository.deleteById(id);
    }

}
