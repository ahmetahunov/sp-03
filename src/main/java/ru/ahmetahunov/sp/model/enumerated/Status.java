package ru.ahmetahunov.sp.model.enumerated;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@RequiredArgsConstructor
public enum Status {

    PLANNED("planned"),
    IN_PROGRESS("in progress"),
    DONE("done");

    @Getter
    @NotNull
    private final String displayName;

}
