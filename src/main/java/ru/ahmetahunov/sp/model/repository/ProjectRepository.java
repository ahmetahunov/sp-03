package ru.ahmetahunov.sp.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ahmetahunov.sp.model.entity.Project;

public interface ProjectRepository extends JpaRepository<Project, String> {

}
