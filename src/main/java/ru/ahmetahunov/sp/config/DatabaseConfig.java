package ru.ahmetahunov.sp.config;

import com.mchange.v2.c3p0.DriverManagerDataSource;
import org.hibernate.cfg.AvailableSettings;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("ru.ahmetahunov.sp.model.repository")
public class DatabaseConfig {

	@Autowired
	private Environment env;

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		@NotNull final LocalContainerEntityManagerFactoryBean sessionFactory = new LocalContainerEntityManagerFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan("ru.ahmetahunov.sp.model");
		sessionFactory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		@NotNull final Properties properties = new Properties();
		properties.setProperty(AvailableSettings.HBM2DDL_AUTO, env.getProperty("hibernate.hbm2ddl.auto"));
		properties.setProperty(AvailableSettings.SHOW_SQL, env.getProperty("hibernate.show_sql"));
		properties.setProperty(AvailableSettings.DIALECT, env.getProperty("hibernate.dialect"));
		properties.setProperty(AvailableSettings.USE_SECOND_LEVEL_CACHE, env.getProperty("hibernate.use_second_level_cache"));
		properties.setProperty(AvailableSettings.USE_QUERY_CACHE, env.getProperty("hibernate.use_query_cache"));
		properties.setProperty(AvailableSettings.USE_MINIMAL_PUTS, env.getProperty("hibernate.use_minimal_puts"));
		properties.setProperty(AvailableSettings.CACHE_PROVIDER_CONFIG, env.getProperty("hibernate.provider_configuration_file"));
		properties.setProperty(AvailableSettings.CACHE_REGION_PREFIX, env.getProperty("hibernate.region_prefix"));
		properties.setProperty(AvailableSettings.CACHE_REGION_FACTORY, env.getProperty("hibernate.region.factory_class"));
		properties.setProperty("hibernate.hazelcast.use_lite_member", env.getProperty("hibernate.hazelcast.use_lite_member"));
		sessionFactory.setJpaProperties(properties);
		return sessionFactory;
	}

	@Bean
	public DriverManagerDataSource dataSource() {
		@NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClass(env.getProperty("db.driver"));
		dataSource.setJdbcUrl(env.getProperty("db.url"));
		dataSource.setUser(env.getProperty("db.login"));
		dataSource.setPassword(env.getProperty("db.password"));
		return dataSource;
	}

	@Bean
	public PlatformTransactionManager transactionManager() {
		@NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return transactionManager;
	}

}
