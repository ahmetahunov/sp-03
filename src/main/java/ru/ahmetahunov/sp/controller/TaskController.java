package ru.ahmetahunov.sp.controller;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import ru.ahmetahunov.sp.model.api.service.IProjectService;
import ru.ahmetahunov.sp.model.api.service.ITaskService;
import ru.ahmetahunov.sp.model.entity.Project;
import ru.ahmetahunov.sp.model.entity.Task;
import java.util.List;

@Controller
@RequestMapping("/task")
public class TaskController {

	@Setter
	@NotNull
	@Autowired
	private ITaskService taskService;

	@Setter
	@NotNull
	@Autowired
	private IProjectService projectService;
	
	@GetMapping("/")
	public String taskList(@NotNull final Model model) {
		model.addAttribute("tasks", taskService.findAll());
		return "task/task_list";
	}

	@GetMapping("/list/{projectId}")
	public String taskListByProjectId(@PathVariable("projectId") @NotNull final String id, @NotNull final Model model) {
		@NotNull final List<Task> tasks = taskService.findAll(id);
		model.addAttribute("tasks", tasks);
		model.addAttribute("project", projectService.findOne(id));
		return "task/task_list_by_project";
	}

	@GetMapping("/{id}")
	public String taskInfo(@PathVariable("id") @NotNull final String id, @NotNull final Model model) {
		model.addAttribute("task", taskService.findOne(id));
		return "task/task_info";
	}

	@GetMapping("/create")
	public String taskCreate(@NotNull final Model model) {
		model.addAttribute("projects", projectService.findAll());
		return "task/task_create";
	}

	@PostMapping("/create")
	public String taskCreate(
			@NotNull final Task task,
			@ModelAttribute("projectId") @NotNull final String projectId
	) throws InterruptedOperationException {
		if (task.getName().trim().isEmpty()) throw new InterruptedOperationException();
		@NotNull final Project project = new Project();
		project.setId(projectId);
		task.setProject(project);
		taskService.persist(task);
		return "redirect:/task/";
	}

	@GetMapping("/update/{id}")
	public String taskUpdate(@PathVariable("id") @NotNull final String id, @NotNull final Model model) throws InterruptedOperationException {
		@Nullable final Task task = taskService.findOne(id);
		if (task == null) throw new InterruptedOperationException();
		model.addAttribute("taskEdit", task);
		model.addAttribute("projectEdit", task.getProject());
		model.addAttribute("projects", projectService.findAll());
		return "task/task_update";
	}

	@PostMapping("/update")
	public String taskUpdate(
			@NotNull final Task task,
			@ModelAttribute("projectId") @NotNull final String projectId,
			@NotNull final Model model
	) throws InterruptedOperationException {
		@Nullable final Task found = taskService.findOne(task.getId());
		if (found == null) throw new InterruptedOperationException();
		if (!task.getName().trim().isEmpty())found.setName(task.getName());
		@NotNull final Project project = new Project();
		project.setId(projectId);
		found.setProject(project);
		found.setDescription(task.getDescription());
		found.setStartDate(task.getStartDate());
		found.setFinishDate(task.getFinishDate());
		found.setStatus(task.getStatus());
		model.addAttribute("task", taskService.merge(found));
		return "task/task_info";
	}

	@GetMapping("/delete/{id}")
	public String taskDelete(@PathVariable("id") @NotNull final String id) throws InterruptedOperationException {
		taskService.remove(id);
		return "redirect:/task/";
	}
	
}
