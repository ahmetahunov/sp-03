package ru.ahmetahunov.sp.controller;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import ru.ahmetahunov.sp.model.api.service.IProjectService;
import ru.ahmetahunov.sp.model.entity.Project;

@Controller
@RequestMapping(value = "/project")
public class ProjectController {

	@Setter
	@NotNull
	@Autowired
	private IProjectService projectService;

	@GetMapping("/")
	public String projectList(@NotNull final Model model) {
		model.addAttribute("projects", projectService.findAll());
		return "project/project_list";
	}

	@GetMapping("/{id}")
	public String projectInfo(@PathVariable("id") @NotNull final String id, @NotNull final Model model) {
		model.addAttribute("project", projectService.findOne(id));
		return "project/project_info";
	}

	@GetMapping("/create")
	public String projectCreate() {
		return "project/project_create";
	}

	@PostMapping("/create")
	public String projectCreate(@NotNull final Project project) throws InterruptedOperationException {
		if (project.getName().trim().isEmpty()) throw new InterruptedOperationException();
		projectService.persist(project);
		return "redirect:/project/";
	}

	@GetMapping("/update/{id}")
	public String projectUpdate(@PathVariable("id") @NotNull final String id, @NotNull final Model model) {
		model.addAttribute("projectEdit", projectService.findOne(id));
		return "project/project_update";
	}

	@PostMapping("/update")
	public String projectUpdate(@NotNull final Project project, @NotNull final Model model) throws InterruptedOperationException {
		@Nullable final Project found = projectService.findOne(project.getId());
		if (found == null) throw new InterruptedOperationException();
		if (!project.getName().isEmpty())found.setName(project.getName());
		found.setDescription(project.getDescription());
		found.setStartDate(project.getStartDate());
		found.setFinishDate(project.getFinishDate());
		found.setStatus(project.getStatus());
		model.addAttribute("project", projectService.merge(found));
		return "project/project_info";
	}

	@GetMapping("/delete/{id}")
	public String projectDelete(@PathVariable("id") @NotNull final String id) throws InterruptedOperationException {
		projectService.remove(id);
		return "redirect:/project/";
	}

}
