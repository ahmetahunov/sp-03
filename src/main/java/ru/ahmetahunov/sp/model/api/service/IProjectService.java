package ru.ahmetahunov.sp.model.api.service;

import ru.ahmetahunov.sp.model.entity.Project;

public interface IProjectService extends IAbstractService<Project> {

}
